import React, { Component } from "react";
import axios from "../../axios-base";
import Restaurant from "../../components/Restaurant/Restaurant";
import 'bootstrap/dist/css/bootstrap.min.css';

class RestaurantBuilder extends Component {
  state = {
    restaurants: []
  };

  componentDidMount() {

    axios
      .get("Restaurant")
      .then(response => {
        if (response.data != null) {
          this.setState({ restaurants: response.data });
        } else {
          console.log("Error");
        }
      })
      .catch(error => {
        console.log(error);
      });
  }



  render() {
    return (
      <div className="container">
        {this.state.restaurants.map(restaurant => {
          return (
            <div>
              <Restaurant
                key={restaurant.id}
                id={restaurant.id}
                name={restaurant.name}
                description={restaurant.description}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

export default RestaurantBuilder;
