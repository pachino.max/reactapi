import React, { Component } from "react";
import axios from "../../axios-base";
import Dish from "../../components/Dish/Dish";
import 'bootstrap/dist/css/bootstrap.min.css';

class DishBuilder extends Component {
  state = {
    dishes: []
  };

  componentDidMount() {
    
    axios
      .get("Dish")
      .then(response => {
        if (response.data != null) {
          this.setState({ dishes: response.data });
        } else {
          console.log("Error");
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  

  render() {
    return (
      <div className="container">
        {this.state.dishes.map(dish => {
          return (
            <div >
              <Dish
                key={dish.id}
                id={dish.id}
                description={dish.description}
                name={dish.name}
                price={dish.price}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

export default DishBuilder;
