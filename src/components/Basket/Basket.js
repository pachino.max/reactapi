import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

function Basket(props) {
  return (
    <div className="card text align left">
      <div>Наименование: {props.name}</div>
      <div>Цена: {props.price}</div>
    </div>
  );
}

export default Basket;
