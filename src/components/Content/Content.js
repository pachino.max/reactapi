import React from 'react'
import {BrowserRouter,Switch, Route } from 'react-router-dom'
import Menu from '../Menu/Menu'
import 'bootstrap/dist/css/bootstrap.min.css';
import RestaurantBuilder from '../../containers/RestaurantBulder/RestaurantBuilder'
import DishBuilder from '../../containers/DishBuilder/DishBuilder'



const Content = () =>{
    return(
      <BrowserRouter>
      
      <Menu />
      <Switch>
        
        <Route path="/restaurants" component={RestaurantBuilder}/>
        {/* <Route path="/home" component={BasketBuilder}/> */}
        <Route path="/dishes" component={DishBuilder}/>
      </Switch>
      </BrowserRouter>
    )
  }
   
  export default Content 