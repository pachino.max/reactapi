import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

function Dish(props) {
  return (
    <div className="card">
      <div>Название: {props.name}</div>
      <div>Описание: {props.description}</div>
      <div>Цена: {props.price} com</div>
      <button className='danger' onClick={props.addToBasket}>Add</button>
    </div>
  );
}

export default Dish;
