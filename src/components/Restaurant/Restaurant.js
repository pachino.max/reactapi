import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

function Restaurant(props) {
    return (
        <div className="container">

            <div className="card row">
                <div className="card-body">Кафе: {props.name}</div>
                <div className="card-title">Адрес: {props.description}</div>
            </div>
        </div>
    );
}

export default Restaurant
