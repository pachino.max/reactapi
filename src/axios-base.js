import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:54772/api/"
});

export default instance;